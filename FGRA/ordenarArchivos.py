import os, shutil


def extensionesDeArchivos(rutaInicial='./calidadAguaFGRA'):
    tipos = dict()
    for root, dirs, files in os.walk(rutaInicial):
        for file in files:
            filename, fileExt = os.path.splitext(file)
            if fileExt in tipos:
                tipos[fileExt] += 1
            else:
                tipos[fileExt] = 1
    return tipos


print(extensionesDeArchivos('./Calidad_Agua_FGRA'))


# '.xlsx': 267, '.pdf': 439, '.zip': 2, '.docx': 8, '.PDF': 7, '.pptx': 2}
# total: 728 archivos
# {'.xlsx': 305, '.pdf': 439, '.zip': 2, '.docx': 8, '.PDF': 7, '.pptx': 2}
# TOTAL: 760 archivos
# se descuentan el xlsx del Listado de proyectos y los 2 zip
# Archivos ZIP
# ./calidadAguaFGRA/0413_FONNOR/Entregables Consultoría de Muestreo/5. Informes de resultados de aforos/
# Informes Octubre 2021/Aforos/27-Mediciones rio Arronte 12-nov-2021.zip
# ./calidadAguaFGRA/0496_SuMar/Inf 2/A496_Informe2_R2_A4_Monitoreo agua.zip


# Nueva versión de la carpeta proporcionada por Juan José
# {'.xlsx': 22, '.pdf': 145, '.PDF': 7, '.pptx': 2, '.docx': 1, '.zip': 1}
# Después de extraer el zip:
# {'.xlsx': 32, '.pdf': 145, '.PDF': 7, '.pptx': 2, '.docx': 1, '.zip': 1}

