#! /usr/bin/env python3

import os, re, pandas as pd
from pprint import pprint


def extraerXlsxTres(rutaInicial='./calidadAguaFGRA', buscar='ph'):
    import openpyxl
    parametros = dict()
    registro = pd.DataFrame(columns=['ruta', 'archivo', 'hoja', 'encontrados'])
    # USO DE REGEX PARA LIMPIAR LOS NOMBRES DE PARÁMETROS QUE TIENEN UNIDADES
    paramRegex = re.compile(r'(.*)(\(\w+\W\w+\))?')
    # RECORRER EL DIRECTORIO DE BÚSQUEDA
    for root, dirs, files in os.walk(rutaInicial):
        for file in files:
            filename, fileExt = os.path.splitext(file)
            # FILTRAR ARCHIVOS DE EXCEL EN EL DIRECTORIO DE BÚSQUEDA
            if fileExt in ('.xlsx', '.xls'):
                print(root+'/'+file)  # # # # # # # # MOSTRAR DIRECCIÓN DEL ARCHIVO EN TURNO
                # ABRIR ARCHIVO
                libro = openpyxl.load_workbook(os.path.join(root, file), data_only=True)
                # ITERAR HOJAS
                for hoja in libro.worksheets:
                    print(f'>>> {hoja.title}')  # # # # # # # # MOSTRAR NOMBRE DE LA HOJA EN TURNO
                    contador = 0
                    numFilas = list()
                    numCols = list()
                    # ITERAR FILAS PARA GUARDAR UBICACIÓN DE TODAS LAS APARICIONES DE LA CADENA buscar
                    for fila in hoja.iter_rows():
                        for celda in fila:
                            if type(celda.value) is str and buscar in celda.value.lower():
                                numFilas.append(celda.row)
                                numCols.append(celda.column)
                                print(f'{buscar} encontrado...')
                    if len(numFilas) > 0:
                        # ITERAR TODAS LAS UBICACIONES GUARDADAS PARA MOSTRAR AL USUARIO EL CONTENIDO DE LA FILA Y COLUMNA CORRESPONDIENTES
                        for i, j in zip(numFilas, numCols):
                            paraFila = list()
                            paraCol = list()
                            trasladar = list()
                            # COPIAR CONTENIDO DE LA FILA EN UNA LISTA AUXILIAR
                            for celda in list(hoja.rows)[i-1]:
                                if type(celda.value) is str:
                                    param = paramRegex.search(celda.value)
                                    paraFila.append(param.group(1).lower())
                            # COPIAR CONTENIDO DE LA COLUMNA EN UNA LISTA AUXILIAR
                            for celda in list(hoja.columns)[j-1]:
                                if type(celda.value) is str:
                                    param = paramRegex.search(celda.value)
                                    paraCol.append(param.group(1).lower())
                            # INTERACCIÓN CON EL USUARIO
                            # MOSTRAR CONTENIDO DE LA FILA
                            print('El contenido de la fila es:')
                            for num, item in enumerate(paraFila):
                                print(f'{num}: {item}')
                            # MOSTRAR CONTENIDO DE LA COLUMNA
                            print('El contenido de la columna es:')
                            for num, item in enumerate(paraCol):
                                print(f'{num}: {item}')
                            # ENTRADA DE USUARIO: FILA, COLUMNA U OMITIR Y CONTINUAR CON EL ALGORITMO
                            vector = input('¿Usar fila (f), columna (c), o pasar a al siguiente (s)?')
                            if vector != 's':
                                # ENTRADA DE USUARIO: RANGO DE PARÁMETROS IDENTIFICADOS
                                ind1 = int(input('Indica el índice del primer parámetro'))
                                ind2 = int(input('Indica el índice del último parámetro'))
                            # COPIAR DATOS DE LA LISTA AUXILIAR CORRESPONDIENTE USANDO OTRA LISTA INTERMEDIARIA
                                if vector == 'f':
                                    trasladar = paraFila[ind1:ind2+1]
                                elif vector == 'c':
                                    trasladar = paraCol[ind1:ind2+1]
                                # GUARDAR PARÁMETROS
                                for item in trasladar:
                                    if item in parametros:
                                        parametros[item] += 1
                                        contador += 1
                                    else:
                                        parametros[item] = 1
                                        contador += 1
                            print(parametros)  # # # # # # # # MOSTRAR LISTA ACTUALIZADA DE PARÁMETROS
                    registro = pd.concat([pd.DataFrame([[root, file, hoja.title, contador]], columns=registro.columns), registro], ignore_index=True)
                libro.close()
    pd.DataFrame(parametros.items(), columns=['nombre', 'repeticiones']).to_csv('./parametros_xlsx_jjpi_'+buscar+'.csv')
    registro.to_excel('./reporte_xlsx_jjpi_'+buscar+'.xlsx')
    return parametros


def extraerPdfDos(rutaInicial='./pdf_por_revisar', buscar='ph'):
    from tabula import read_pdf
    parametros = dict()
    registro = pd.DataFrame(columns=['ruta', 'archivo', 'tabla', 'encontrados'])
    # USO DE REGEX PARA LIMPIAR LOS NOMBRES DE PARÁMETROS QUE TIENEN UNIDADES
    paramRegex = re.compile(r'(.*)(\(\w+\W\w+\))?')
    for root, dirs, files in os.walk(rutaInicial):
        for file in files:
            filename, fileExt = os.path.splitext(file)
            # SELECCIONAR UN PDF EN ESPECÍFICO
            if filename == '6o informe calidad del agua Sitala.pdf':
            # FILTRAR ARCHIVOS PDF EN EL DIRECTORIO DE BÚSQUEDA
            # if fileExt.lower() == '.pdf':
                print(root+'/'+file)  # # # # # # # # MOSTRAR DIRECCIÓN DEL ARCHIVO EN TURNO
                # ABRIR ARCHIVO Y DETECTAR TABLAS
                tablas = read_pdf(os.path.join(root, file), pages='all')  # address of pdf file
                if len(tablas) > 0:
                    numTabla = 0
                    # ITERAR TABLAS DETECTAS
                    for tabla in tablas:
                        numTabla += 1
                        contador = 0
                        # BUSCAR LA CADENA buscar ENTRE LOS NOMBRES DE COLUMNA DEL DATAFRAME
                        if any(buscar in item.lower() for item in list(tabla.columns)):
                            paraFila = list()
                            for item in list(tabla.columns):
                                param = paramRegex.search(item)
                                paraFila.append(param.group(1).lower())
                            # MOSTRAR CONTENIDO DE LA FILA
                            print('Los encabezados son:')
                            for num, item in enumerate(paraFila):
                                print(f'{num}: {item}')
                            # ENTRADA DE USUARIO: EXISTEN PARÁMETROS U OMITIR Y CONTINUAR CON EL ALGORITMO
                            vector = input('¿Hay parámetros?   [s]í   [n]o')
                            if vector == 's':
                                # ENTRADA DE USUARIO: RANGO DE PARÁMETROS IDENTIFICADOS
                                ind1 = int(input('Indica el índice del primer parámetro'))
                                ind2 = int(input('Indica el índice del último parámetro'))
                                trasladar = paraFila[ind1:ind2 + 1]
                                # GUARDAR PARÁMETROS
                                for item in trasladar:
                                    if item in parametros:
                                        parametros[item] += 1
                                        contador += 1
                                    else:
                                        parametros[item] = 1
                                        contador += 1
                        # ITERAR COLUMNAS PARA IDENTIFICAR TODAS LAS APARICIONES DE LA CADENA buscar
                        for columna in tabla:
                            texto = ''
                            for item in list(tabla[columna]):
                                if type(item) is str:
                                    texto += item.lower() + ' | '
                            if buscar in texto:
                                paraCol = list()
                                for item in list(tabla[columna]):
                                    if type(item) is str:
                                        param = paramRegex.search(item)
                                        paraCol.append(param.group(1).lower())
                                # MOSTRAR CONTENIDO DE LA COLUMNA
                                print('El contenido de la columna es:')
                                for num, item in enumerate(paraCol):
                                    print(f'{num}: {item}')
                                # ENTRADA DE USUARIO: EXISTEN PARÁMETROS U OMITIR Y CONTINUAR CON EL ALGORITMO
                                vector = input('¿Hay parámetros?   [s]í   [n]o')
                                if vector == 's':
                                    # ENTRADA DE USUARIO: RANGO DE PARÁMETROS IDENTIFICADOS
                                    ind1 = int(input('Indica el índice del primer parámetro'))
                                    ind2 = int(input('Indica el índice del último parámetro'))
                                    trasladar = paraCol[ind1:ind2 + 1]
                                    # GUARDAR PARÁMETROS
                                    for item in trasladar:
                                        if item in parametros:
                                            parametros[item] += 1
                                            contador += 1
                                        else:
                                            parametros[item] = 1
                                            contador += 1
                        print(parametros)  # # # # # # # # MOSTRAR LISTA ACTUALIZADA DE PARÁMETROS
                    registro = pd.concat([pd.DataFrame([[root, file, numTabla, contador]], columns=registro.columns), registro], ignore_index=True)
                else:
                    print('ño')
    pd.DataFrame(parametros.items(), columns=['nombre', 'repeticiones']).to_csv('./parametros_pdf_todos_' + buscar + '.csv')
    registro.to_excel('./reporte_pdf_todos_' + buscar + '.xlsx')
    return parametros


# pprint(extraerXlsxTres('./Calidad_Agua_FGRA', buscar='cadmio'))
# extraerPdf('/home/ppopoca/PycharmProjects/FGRA2409/calidadAguaFGRA/0419_SuMar')
# extraerPdf('./calidadAguaFGRA')
# extraerPdf('./calidadAguaFGRA', 'temp')
extraerPdfDos()
