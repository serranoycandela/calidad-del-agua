# Análisis de bases de datos

## Información recolectada

A continuación se listan las variables recolectadas de los diferentes ejemplos de muestras provistos, el trabajo previo de LANCIS y la respuesta de la FGRA. Columnas de nombres distintos con el mismo tipo de información se reducen y normalizan.

### Datos

| Variable | Unidad | Columna normalizada |
| - | - | - |
| Amonio | mg/L | `amonio` |
| Arsénico | mg/L | `arsenico` |
| Cadmio | mg/L | `cadmio` |
| Cafeína | na | `cafeina` |
| Cloro | mg/L | `cloro` |
| Clorofila a | mg/m3 | `clorofila_a` |
| Cobre | mg/L | `cobre` |
| Colformes fecales | UFC/100 mL | `colformes_fecales` |
| Color verdadero | "nm, UPtCo" | `color_verdadero` |
| Conductividad eléctrica | mS/cm = 1mmho/cm | `conductividad_electrica` |
| Cromo | mg/L | `cromo` |
| Demanda Bioquímica de Oxígeno | mg/L | `demanda_bioquimica_de_oxigeno` |
| Demanda Química de Oxígeno | mg/L | `demanda_quimica_de_oxigeno` |
| Derivados de combustibles | mg/L | `derivados_de_combustibles` |
| Derivados de solventes | mg/L | `derivados_de_solventes` |
| Dureza | mg/L | `dureza` |
| Enterococos fecales | UFC/100 mL | `enterococos_fecales` |
| Escherichia coli | UFC/100 mL | `escherichia_coli` |
| Fierro | mg/L | `fierro` |
| Fluoruro | mg/l (ppm) | `fluoruro` |
| Fósforo | mg/L | `fosforo` |
| Giardia lamblia | quistes/20L | `giardia_lamblia` |
| Grasas y aceites | mg/L | `grasas_y_aceites` |
| Material flotante | NA | `material_flotante` |
| Mercurio | mg/L | `mercurio` |
| Microcystina-LR | mg/L | `microcystina_lr` |
| Niquel | mg/L | `niquel` |
| Nitratos | mg/L | `nitratos` |
| Nitritos | mg/L | `nitritos` |
| Nitrogeno amoniacal | mg/l | `nitrogeno_amoniacal` |
| Nitrógeno | mg/L | `nitrogeno` |
| Ortofosfatos | mg/L | `ortofosfatos` |
| Oxígeno | mg/L | `oxigeno` |
| PH | 1-14 | `ph` |
| Plomo | mg/L | `plomo` |
| Saam | na | `saam` |
| Salinidad | %o | `salinidad` |
| Silice reactiva | mg/l | `silice_reactiva` |
| Sulfatos | mg/l | `sulfatos` |
| Sólidos disueltos | mg/L | `solidos_disueltos` |
| Sólidos suspendidos | mg/L | `solidos_suspendidos` |
| Temperatura | °C | `temperatura` |
| Trihalometanos | mg/L | `trihalometanos` |
| Trix | na | `trix` |
| Turbidez | UFN | `turbidez` |
| Zinc | mg/L | `zinc` |

### Metadatos

Información encontrada que describe la muestra, sin ser información cuantitativa sobre la misma.

| Variable | Tipo de dato | Descripción |
| - | - | - |
| ubicacion | `geometry(point)` | Referencia geográfica del lugar de la muestra |
| sitio | `string` | nombre del sitio de muestreo |
| fecha | `timestamp` | fecha en que fue tomada la muestra (con hora) |
| ecosistema | `string` | ecosistema del sitio donde se colectó la muestra |
| color | `string` | ver documento a.0442_estcalidadagua.pdf |
| olor | `string` | ver documento a.0442_estcalidadagua.pdf |
| sabor | `string` | ver documento a.0442_estcalidadagua.pdf |
| fuente de abastecimiento | `string` |
| usos del agua | `string` |
| condiciones de la fuente de abastecimiento | `string` |
| responsable | `string` | persona que tomó la muestra |

### Datos no reconocidos

Se encontraron en los ejemplos, pero se desconoce su significado.

* orp
* cla (μg/l)

## Propuestas de organización de la base de datos

### Tabla expandida

La información se almacena en una sola tabla (llamémosla `sample`) cuyos registros representan una muestra cada uno, con columnas describiendo cada valor tomado de la muestra (por ejemplo temperatura, concentración de oxígeno, PH, etc.)

| id | timestamp | metadata1 | ph | nitratos | .. |
| - | - | - | - | - | - |
| 1 | 2023-01-24 11:53:12Z | val1 | 6 | 0.0012 | .. |
| 2 | 2023-01-24 11:55:12Z | val2 | 7 | 0.0071 | .. |
| 3 | 2023-01-25 11:55:12Z | val3 | 5 | 0.0004 | .. |


#### Ventajas

* Cada renglón es una unidad geográfica con toda la información de los parámetros que se midieron, y en ese sentido es mas directa su representación geográfica.
* Las consultas para cálculos sobre las muestras son directas.

#### Desventajas

* Si las muestras solo miden algunos parámetros, la tabla estará llena de valores nulos de los parámetros que no se midieron


### Formato long

Se tiene una tabla `sample` cuyos registros tienen solo características de identificación del mismo y algunos metadatos (e.g. fecha de la muestra) y los valores de la muestra se guardan en una segunda tabla `values` con las columnas `sample_id`, `variable`, `value` y cada renglón representa un valor de la muestra (e.g. concentración de oxígeno o PH)

**Tabla sample**

| id | timestamp | metadata1 |
| - | - | - |
| 1 | 2023-01-24 11:53:12Z | val1 |
| 2 | 2023-01-24 11:55:12Z | val2 |
| 3 | 2023-01-25 11:55:12Z | val3 |

**Tabla value**

| sample_id | variable | value |
| - | - | - |
| 1 | pH | 7 |
| 1 | temperatura | 22.2 |
| 2 | pH | 5 |
| 2 | nitritos | 33.3 |

#### Ventajas

* En el caso de información dispersa solo se guardan valores para variables de las que se tiene registro, ahorrando espacio de almacenamiento.

#### Desventajas

* Las consultas para cálculos sobre los valores de las muestras son indirectas.
* Se debe tomar una decisión general sobre el tipo de dato con el que se almacenan los valores puesto que todos los registros deben tener el mismo tipo de dato en la tabla `values`. Pero parece natural que el tipo de dato sea de punto flotante pues salvo el ecosistema, el color, olor y sabor todas las variables que se miden se reportan en punto flotante.

### Columna JSON

Se tiene una sola tabla `sample` cuyos registros representan una muestra y los valores de la misma se almacenan en una única columna (todos) de tipo JSON serializados.

| id | timestamp | metadata1 | values |
| - | - | - | - |
| 1 | 2023-01-24 11:53:12Z | val1 | `{"ph": 7, "nitratos": 0.15}` |
| 2 | 2023-01-24 11:55:12Z | val2 | `{"nitratos": 15, "concentracion_oxigeno": .023}` |

#### Ventajas

* Cada valor de la muestra puede tener su propio tipo de dato
* No se desperdicia espacio de almacenamiento pues solo se almacenan valores capturados

#### Desventajas

* Existe cierta indirección para utilizar los valores para cálculos sobre las muestras pues es necesario deserializar el JSON (sin embargo es importante mencionar que la base de datos ofrece facilidades para hacer esto pues tiene soporte completo de columnas JSON)
* Si la información no es dispersa este tipo de almacenamiento resulta de hecho más pesado que la alternativa de tabla expandidavariable,unidades
